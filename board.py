import dudraw
import random
import AIplayer

class Tictactoe():
    def __init__(self) -> None:
        self.board = [[0 for j in range(3)] for i in range(3)]
        self.current_player = "X"
        self.tie = False
        self.winner = ""

    def print_board(self):
        for col in self.board:
            for row in col:
                print(row, end = " ")
            print()
    
    def draw_board(self):
        dudraw.set_pen_width(7)

        dudraw.line(100, 0, 100, 300)
        dudraw.line(200, 0, 200, 300)
        dudraw.line(0, 100, 300, 100)
        dudraw.line(0, 200, 300, 200)
        dudraw.line(0, 300, 300, 300)

        dudraw.set_font_size(80)
        dudraw.text(150, 325, "Tic-Tac-Toe")

        for i in range(len(self.board)):
            for j in range(len(self.board[i])):
                if self.board[i][j] != 0:
                    dudraw.text((i+1) * 100 - 50, (j+1) * 100 - 50, str(self.board[i][j]))

    def random_player(self):
        positon_list = []

        for i in range(3):
            for j in range(3):
                if self.board[i][j] == 0:
                    positon_list.append((i, j))

        if len(positon_list) > 0:
            posistion = random.choice(positon_list)
            self.board[posistion[0]][posistion[1]] = self.current_player

    def AIPlayer(self):
        move = AIplayer.compute_move(self.board)
        print(move)
        self.board[move[0][0]][move[0][1]] = self.current_player

    def check_click(self, x, y):
            if self.board[x][y] == 0:
                self.board[x][y] = self.current_player

    def swap_players(self):
        if self.current_player == "X":
            self.current_player = "O"
        else:
            self.current_player = "X"

    def check_win(self):
        for i in range(3):
            if self.board[0][i] != 0:
                start_square = self.board[0][i]
                if self.board[1][i] == start_square and self.board[2][i] == start_square:
                    self.winner = start_square
                    return False

        for i in range(3):
            if self.board[i][0] != 0:
                start_square = self.board[i][0]
                if self.board[i][1] == start_square and self.board[i][2] == start_square:
                    self.winner = start_square
                    return False
        
        if self.board[1][1] != 0:
            start_square = self.board[1][1]
            if self.board[0][0] == start_square and self.board[2][2] == start_square:
                self.winner = start_square
                return False
            if self.board[0][2] == start_square and self.board[2][0] == start_square:
                self.winner = start_square
                return False
 
        for row in self.board:
            for num in row:
                if num == 0:
                    return True
                
        self.tie = True
        return False