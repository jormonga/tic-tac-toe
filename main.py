import board
import dudraw

def main():
    dudraw.set_canvas_size(600, 700)
    dudraw.set_x_scale(0, 300)
    dudraw.set_y_scale(0, 350)
    
    new_Tictactoe = board.Tictactoe()

    new_Tictactoe.AIPlayer()
    new_Tictactoe.swap_players()

    while new_Tictactoe.check_win():
        if dudraw.mouse_clicked():
            x = int(dudraw.mouse_x()//100)
            y = int(dudraw.mouse_y()//100)
            new_Tictactoe.check_click(x, y)
            new_Tictactoe.swap_players()
            new_Tictactoe.AIPlayer()
            new_Tictactoe.swap_players()
            dudraw.clear()
        else:
            new_Tictactoe.draw_board()
            dudraw.show(20)

    if new_Tictactoe.tie:
        print('Tie!')
    else:
        print(f"{new_Tictactoe.winner} wins the game!")
    new_Tictactoe.draw_board()
    dudraw.show(float('inf'))

if __name__ == "__main__":
    main()