import copy
import random

def terminal(gamestate):
    for i in range(3):
        if gamestate[0][i] != 0:
            start_square = gamestate[0][i]
            if gamestate[1][i] == start_square and gamestate[2][i] == start_square:
                return True

    for i in range(3):
        if gamestate[i][0] != 0:
            start_square = gamestate[i][0]
            if gamestate[i][1] == start_square and gamestate[i][2] == start_square:
                return True
    
    if gamestate[1][1] != 0:
        start_square = gamestate[1][1]
        if gamestate[0][0] == start_square and gamestate[2][2] == start_square:
            return True
        if gamestate[0][2] == start_square and gamestate[2][0] == start_square:
            return True

    for row in gamestate:
        for num in row:
            if num == 0:
                return False
        
    return True

def score(gamestate):
    for i in range(3):
        if gamestate[0][i] != 0:
            start_square = gamestate[0][i]
            if gamestate[1][i] == start_square and gamestate[2][i] == start_square:
                if start_square == "X":
                    return 1
                else:
                    return -1

    for i in range(3):
        if gamestate[i][0] != 0:
            start_square = gamestate[i][0]
            if gamestate[i][1] == start_square and gamestate[i][2] == start_square:
                if start_square == "X":
                    return 1
                else:
                    return -1
    
    if gamestate[1][1] != 0:
        start_square = gamestate[1][1]
        if gamestate[0][0] == start_square and gamestate[2][2] == start_square:
            if start_square == "X":
                return 1
            else:
                return -1
        if gamestate[0][2] == start_square and gamestate[2][0] == start_square:
            if start_square == "X":
                return 1
            else:
                return -1
    
    return 0

def player(gamestate):
    total = 0

    for row in gamestate:
        for num in row:
            if num != 0:
                total += 1

    if total % 2 == 0:
        return "MAX"
    else:
        return "MIN"

def action(gamestate):
    possible_actions = []

    positon_list = []

    for i in range(3):
        for j in range(3):
            if gamestate[i][j] == 0:
                positon_list.append((i, j))

    if player(gamestate) == "MAX":
        for pos in positon_list:
            possible_actions.append((pos, "X"))
    else:
        for pos in positon_list:
            possible_actions.append((pos, "O"))

    return possible_actions

def result(gamestate, action):
    state = copy.deepcopy(gamestate)
    state[action[0][0]][action[0][1]] = action[1]
    return state

def Minimax(gamestate):
    if terminal(gamestate):
        return score(gamestate)
    
    if player(gamestate) == "MAX":
        value = -(float('inf'))
        for a in action(gamestate):
            value = max(value, Minimax(result(gamestate, a)))
        return value
    
    if player(gamestate) == "MIN":
        value = float('inf')
        for a in action(gamestate):
            value = min(value, Minimax(result(gamestate, a)))
        return value

def compute_move(gamestate):
    one_moves = []
    zero_moves = []
    neg_moves = []

    actions = action(gamestate)
    
    result_table = []
    for a in actions:
        result_table.append(result(gamestate, a))

    for i in range(len(actions)):
        if Minimax(result_table[i]) == 1:
            one_moves.append(actions[i])
        elif Minimax(result_table[i]) == 0:
            zero_moves.append(actions[i])
        else:
            neg_moves.append(actions[i])
            
    if player(gamestate) == "MAX":
        if len(one_moves)>0:
            return random.choice(one_moves)
        elif len(zero_moves)>0:
            return random.choice(zero_moves)
        else:
            return random.choice(neg_moves)
        
    else:
        if len(neg_moves)>0:
            return random.choice(neg_moves)
        elif len(zero_moves)>0:
            return random.choice(zero_moves)
        else:
            return random.choice(one_moves)
        
def main():
    gamestate = [[0, 0, "X"],
                 ["O", 0, 0],
                 [0, 0, 0]]
    print(compute_move(gamestate))

if __name__ == "__main__":
    main()